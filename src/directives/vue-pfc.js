import { debounce } from 'lodash';

const callback = (binding = {}) => {
  const { modifiers, value } = binding;
  return debounce(
    e => {
      if (modifiers.stop) {
        e.stopPropagation();
      } else if (modifiers.prevent) {
        e.preventDefault();
      }
      if (typeof value === 'function') {
        value(e);
      } else {
        const { callback } = value;
        typeof callback === 'function' && callback(e);
      }
    },
    value && (value.wait || 500),
    {
      leading: true,
      trailing: false,
    },
  );
};

// 回调函数仓库
let callbackRepo = () => {};
/**
 * 自定义指令，处理连续点击问题
 * v-pfc='{wait: 300, callback: callbackFunc}'
 * v-pfc='callBackFun'
 */

export default {
  inserted: function(el, binding) {
    callbackRepo = callback(binding);
    el.addEventListener('click', callbackRepo);
  },
  unbind: function(el) {
    el.removeEventListener('click', callbackRepo);
  },
};
