import { heroTree } from '@/data/hero';

let typeList = ['暴击', '攻击加成', '首领御魂', '效果命中', '效果抵抗', '生命加成', '防御加成'];

let yuhunTree = [
  {
    label: '暴击',
    value: 'CritRate',
    children: [
      {
        label: '破势',
        value: '破势',
      },
      {
        label: '针女',
        value: '针女',
      },
      {
        label: '网切',
        value: '网切',
      },
      {
        label: '三味',
        value: '三味',
      },
      {
        label: '伤魂鸟',
        value: '伤魂鸟',
      },
      {
        label: '镇墓兽',
        value: '镇墓兽',
      },
      {
        label: '青女房',
        value: '青女房',
      },
    ],
  },
  {
    label: '攻击加成',
    value: 'AttackRate',
    children: [
      {
        label: '心眼',
        value: '心眼',
      },
      {
        label: '狰',
        value: '狰',
      },
      {
        label: '轮入道',
        value: '轮入道',
      },
      {
        label: '狂骨',
        value: '狂骨',
      },
      {
        label: '鸣屋',
        value: '鸣屋',
      },
      {
        label: '蝠翼',
        value: '蝠翼',
      },
      {
        label: '阴摩罗',
        value: '阴摩罗',
      },
      {
        label: '兵主部',
        value: '兵主部',
      },
    ],
  },
  {
    label: '首领御魂',
    value: '首领御魂',
    children: [
      {
        label: '荒骷髅',
        value: '荒骷髅',
      },
      {
        label: '蜃气楼',
        value: '蜃气楼',
      },
      {
        label: '土蜘蛛',
        value: '土蜘蛛',
      },
      {
        label: '地震鲶',
        value: '地震鲶',
      },
      {
        label: '胧车',
        value: '胧车',
      },
      {
        label: '鬼灵歌伎',
        value: '鬼灵歌伎',
      },
    ],
  },
  {
    label: '效果命中',
    value: 'EffectHitRate',
    children: [
      {
        label: '火灵',
        value: '火灵',
      },
      {
        label: '蚌精',
        value: '蚌精',
      },
      {
        label: '飞缘魔',
        value: '飞缘魔',
      },
    ],
  },
  {
    label: '效果抵抗',
    value: 'EffectResistRate',
    children: [
      {
        label: '返魂香',
        value: '返魂香',
      },
      {
        label: '魍魉之匣',
        value: '魍魉之匣',
      },
      {
        label: '幽谷响',
        value: '幽谷响',
      },
      {
        label: '骰子鬼',
        value: '骰子鬼',
      },
    ],
  },
  {
    label: '生命加成',
    value: 'HpRate',
    children: [
      {
        label: '地藏像',
        value: '地藏像',
      },
      {
        label: '薙魂',
        value: '薙魂',
      },
      {
        label: '钟灵',
        value: '钟灵',
      },
      {
        label: '镜姬',
        value: '镜姬',
      },
      {
        label: '树妖',
        value: '树妖',
      },
      {
        label: '被服',
        value: '被服',
      },
      {
        label: '涅槃之火',
        value: '涅槃之火',
      },
      {
        label: '涂佛',
        value: '涂佛',
      },
    ],
  },
  {
    label: '防御加成',
    value: 'DefenseRate',
    children: [
      {
        label: '招财猫',
        value: '招财猫',
      },
      {
        label: '木魅',
        value: '木魅',
      },
      {
        label: '珍珠',
        value: '珍珠',
      },
      {
        label: '魅妖',
        value: '魅妖',
      },
      {
        label: '雪幽魂',
        value: '雪幽魂',
      },
      {
        label: '反枕',
        value: '反枕',
      },
      {
        label: '日女巳时',
        value: '日女巳时',
      },
    ],
  },
];

let mainPropList = [
  {
    text: '二号位',
    id: 2,
    children: [
      {
        text: '攻击加成',
        id: 21,
      },
      {
        text: '生命加成',
        id: 22,
      },
      {
        text: '防御加成',
        id: 23,
      },
      {
        text: '速度',
        id: 24,
      },
    ],
  },
  {
    text: '四号位',
    id: 4,
    children: [
      {
        text: '攻击加成',
        id: 41,
      },
      {
        text: '生命加成',
        id: 42,
      },
      {
        text: '防御加成',
        id: 43,
      },
      {
        text: '效果命中',
        id: 44,
      },
      {
        text: '效果抵抗',
        id: 45,
      },
    ],
  },
  {
    text: '六号位',
    id: 6,
    children: [
      {
        text: '攻击加成',
        id: 61,
      },
      {
        text: '生命加成',
        id: 62,
      },
      {
        text: '防御加成',
        id: 63,
      },
      {
        text: '暴击',
        id: 64,
      },
      {
        text: '暴击伤害',
        id: 65,
      },
    ],
  },
];

let optimizePaneList = [
  {
    name: '输出伤害',
    desc: '输出伤害 = 攻击 × 暴击伤害',
  },
  {
    name: '双重暴击',
    desc: '双重暴击 = 攻击 × 暴击伤害 × 暴击伤害',
  },
  {
    name: '生命治疗',
    desc: '生命治疗 = 生命 × 暴击伤害',
  },
  {
    name: '命抗双修',
    desc: '命抗双修 = 效果命中 + 效果抵抗',
  },
  {
    name: '暴击',
    desc: '-',
  },
  {
    name: '暴击伤害',
    desc: '-',
  },
  {
    name: '攻击',
    desc: '-',
  },
  {
    name: '生命',
    desc: '-',
  },
  {
    name: '防御',
    desc: '-',
  },
  {
    name: '速度',
    desc: '-',
  },
  {
    name: '效果命中',
    desc: '-',
  },
  {
    name: '效果抵抗',
    desc: '-',
  },
];

let mapProp = [
  [
    { label: '攻击加成', value: 'AttackRate' },
    { label: '生命加成', value: 'HpRate' },
    { label: '防御加成', value: 'DefenseRate' },
    { label: '速度', value: 'Speed' },
  ],
  [
    { label: '攻击加成', value: 'AttackRate' },
    { label: '生命加成', value: 'HpRate' },
    { label: '防御加成', value: 'DefenseRate' },
    { label: '效果命中', value: 'EffectHitRate' },
    { label: '效果抵抗', value: 'EffectResistRate' },
  ],
  [
    { label: '攻击加成', value: 'AttackRate' },
    { label: '生命加成', value: 'HpRate' },
    { label: '防御加成', value: 'DefenseRate' },
    { label: '暴击', value: 'CritRate' },
    { label: '暴击伤害', value: 'CritPower' },
  ],
];

let MITAMA_PROPS = [
  '攻击',
  '攻击加成',
  '防御',
  '防御加成',
  '暴击',
  '暴击伤害',
  '生命',
  '生命加成',
  '效果命中',
  '效果抵抗',
  '速度',
];

let MITAMA_ENHANCE = {
  珍珠: { 加成类型: '防御加成', 加成数值: 0.3 },
  骰子鬼: { 加成类型: '效果抵抗', 加成数值: 0.15 },
  蚌精: { 加成类型: '效果命中', 加成数值: 0.15 },
  魅妖: { 加成类型: '防御加成', 加成数值: 0.3 },
  针女: { 加成类型: '暴击', 加成数值: 0.15 },
  返魂香: { 加成类型: '效果抵抗', 加成数值: 0.15 },
  雪幽魂: { 加成类型: '防御加成', 加成数值: 0.3 },
  地藏像: { 加成类型: '生命加成', 加成数值: 0.15 },
  蝠翼: { 加成类型: '攻击加成', 加成数值: 0.15 },
  涅槃之火: { 加成类型: '生命加成', 加成数值: 0.15 },
  三味: { 加成类型: '暴击', 加成数值: 0.15 },
  魍魉之匣: { 加成类型: '效果抵抗', 加成数值: 0.15 },
  被服: { 加成类型: '生命加成', 加成数值: 0.15 },
  招财猫: { 加成类型: '防御加成', 加成数值: 0.3 },
  反枕: { 加成类型: '防御加成', 加成数值: 0.3 },
  轮入道: { 加成类型: '攻击加成', 加成数值: 0.15 },
  日女巳时: { 加成类型: '防御加成', 加成数值: 0.3 },
  镜姬: { 加成类型: '生命加成', 加成数值: 0.15 },
  钟灵: { 加成类型: '生命加成', 加成数值: 0.15 },
  狰: { 加成类型: '攻击加成', 加成数值: 0.15 },
  火灵: { 加成类型: '效果命中', 加成数值: 0.15 },
  鸣屋: { 加成类型: '攻击加成', 加成数值: 0.15 },
  薙魂: { 加成类型: '生命加成', 加成数值: 0.15 },
  心眼: { 加成类型: '攻击加成', 加成数值: 0.15 },
  木魅: { 加成类型: '防御加成', 加成数值: 0.3 },
  树妖: { 加成类型: '生命加成', 加成数值: 0.15 },
  网切: { 加成类型: '暴击', 加成数值: 0.15 },
  阴摩罗: { 加成类型: '攻击加成', 加成数值: 0.15 },
  伤魂鸟: { 加成类型: '暴击', 加成数值: 0.15 },
  破势: { 加成类型: '暴击', 加成数值: 0.15 },
  镇墓兽: { 加成类型: '暴击', 加成数值: 0.15 },
  狂骨: { 加成类型: '攻击加成', 加成数值: 0.15 },
  幽谷响: { 加成类型: '效果抵抗', 加成数值: 0.15 },
  兵主部: { 加成类型: '攻击加成', 加成数值: 0.15 },
  青女房: { 加成类型: '暴击', 加成数值: 0.15 },
  涂佛: { 加成类型: '生命加成', 加成数值: 0.15 },
  飞缘魔: { 加成类型: '效果命中', 加成数值: 0.15 },
  土蜘蛛: { 加成类型: '首领御魂', 加成数值: 0 },
  胧车: { 加成类型: '首领御魂', 加成数值: 0 },
  荒骷髅: { 加成类型: '首领御魂', 加成数值: 0 },
  地震鲶: { 加成类型: '首领御魂', 加成数值: 0 },
  蜃气楼: { 加成类型: '首领御魂', 加成数值: 0 },
  鬼灵歌伎: { 加成类型: '首领御魂', 加成数值: 0 },
};

let MITAMA_PROPS_EFFECT = {
  输出伤害: ['攻击加成', '攻击加成', '攻击', '暴击伤害'],
  双重暴击: ['攻击加成', '攻击加成', '攻击', '暴击伤害'],
  生命治疗: ['生命加成', '暴击伤害'],
  命抗双修: ['效果命中', '效果抵抗'],
  攻击: ['攻击加成', '攻击'],
  生命: ['生命加成', '生命'],
  防御: ['防御加成', '防御'],
  暴击伤害: ['暴击伤害'],
  暴击: ['暴击'],
  速度: ['速度'],
  效果命中: ['效果命中'],
  效果抵抗: ['效果抵抗'],
};

let MITAMA_TYPES = Object.keys(MITAMA_ENHANCE); // ['招财猫', '破势', '荒骷髅', ...]

let HeroTree = [];
for (let o in heroTree) {
  HeroTree.push({ value: o, label: o, children: heroTree[o] });
}

export {
  mainPropList,
  mapProp,
  yuhunTree,
  HeroTree,
  typeList,
  optimizePaneList,
  MITAMA_ENHANCE,
  MITAMA_TYPES,
  MITAMA_PROPS,
  MITAMA_PROPS_EFFECT,
};
