import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';

import mutations from './mutations';

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    username: 'default',
    yuhunList: [],
    yuhunCount: [],
    loading: false,
    yyx: {},
    calcObj: {
      yuhun_list: [],
      shishen: [],
      plan: {},
      l2_prop_limit: [],
      l4_prop_limit: [],
      l6_prop_limit: [],
      limit_props: {},
      limit_pane: {},
      optimize_pane: '输出伤害',
      mode: '4+2',
      shishen_pane: {},
    },
  },
  mutations,
  actions: {},
  getters: {},
  modules: {},
  plugins: [
    createPersistedState({
      key: 'yuhunshow',
      paths: ['yyx'],
      storage: window.sessionStorage,
    }),
  ],
});

export default store;
