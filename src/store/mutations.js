export default {
  updateYhList(state, data) {
    state.yuhunList = data.yuhun;
    state.username = data.username;
  },
  updateYhCount(state, data) {
    state.yuhunCount = data;
  },
  updateYYX(state, data) {
    state.yyx = data;
  },
  updateLoading(state) {
    state.loading = !state.loading;
  },
  updateCalcObj(state, data) {
    state.calcObj[data.key] = data.value;
  },
};
